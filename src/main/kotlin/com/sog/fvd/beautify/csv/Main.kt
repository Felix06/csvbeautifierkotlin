package com.sog.fvd.beautify.csv

import java.io.File
import java.nio.charset.Charset
import java.nio.charset.UnsupportedCharsetException
import kotlin.system.exitProcess

fun printUsageAndExitApp() {
    println("Usage : java -jar beautifyCsv.jar csv_path [encoding]")
    println("   - csv_path: relative or absolute path to csv file")
    println("   - encoding: encoding of original and final file (default UTF-8)")
    exitProcess(0)
}

fun beautifyMyCsv(csvFile: File, newFile: File, charset: Charset) {

    val bufferedWriter = newFile.bufferedWriter(charset)
    val bufferedReader = csvFile.bufferedReader(charset)
    var precedentSpecName = ""
    var line : String?
    var lastLineEmpty = true

    do {

        line = bufferedReader.readLine()

        if(line != null) {
            if (line.startsWith("#") || line == "") {
                bufferedWriter.write(line)
                lastLineEmpty = true
            } else {

                var csvLineList = line.split(",")
                var lineToWrite = line
                if (csvLineList[0].contains("#")) {
                    csvLineList = csvLineList.toMutableList()
                    csvLineList[0] = csvLineList[0].replace("\"", "")
                    lineToWrite = csvLineList.joinToString()
                }

                //Specific to Spec.csv
                if (csvLineList.size > 1) {
                    val specName = csvLineList[1]
                    if (specName != precedentSpecName && precedentSpecName != "") {
                        if(!lastLineEmpty) {
                            bufferedWriter.newLine()
                        }
                    }
                    precedentSpecName = specName
                }

                bufferedWriter.write(lineToWrite)
                lastLineEmpty= false
            }

            bufferedWriter.newLine()
        }

    } while(line != null)

    bufferedReader.close()
    bufferedWriter.close()
}

fun main(args: Array<String>) {

    if(args.size > 2 || args.isEmpty()) {
        println("Wrong number of arguments ! Exiting app...")
        printUsageAndExitApp()
    }

    if(args.contains("--help")) {
        printUsageAndExitApp()
    }

    var charset = Charsets.UTF_8
    if(args.size == 2) {
        try {
            charset = charset(args[1])
        } catch(uce: UnsupportedCharsetException) {
            uce.printStackTrace()
            printUsageAndExitApp()
        }
    }

    val originalFileName = args[0]
    var csvFile = File(originalFileName)
    if(!csvFile.exists()) {
        println("File $originalFileName doesn't exist. Exiting app...")
        printUsageAndExitApp()
    }

    val backupName = "$originalFileName.bkp"
    csvFile.renameTo(File(backupName))
    csvFile = File(backupName)

    val newFile = File(originalFileName)
    if(newFile.exists()) {
        println("Error file $originalFileName still exist it might be lock by another process. Quitting ...")
        printUsageAndExitApp()
    }
    newFile.createNewFile()
    newFile.setWritable(true)

    beautifyMyCsv(csvFile, newFile, charset)

    println("Finished beautify csv : $originalFileName with success ! " +
            "Check that everything is as expected before deleting : $backupName .")

}

